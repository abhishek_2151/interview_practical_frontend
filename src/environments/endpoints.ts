export const endPoints = {
    userLogin: '/user/login',
    addAudio: '/audio/add',
    getAllAudio: '/audio'
}