export const constVariable = {
    SUCCESS: 'SUCCESS',
    ERROR: 'ERROR',
    BEARER: 'Bearer',
    APPLICATION_JSON: 'application/json',
}