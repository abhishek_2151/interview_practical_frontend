import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { regex } from "src/environments/regex";
import { constVariable } from 'src/environments/const';
import { AuthService } from 'src/app/core/http/api/auth.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isSubmitted: boolean = false


  constructor(private fb: FormBuilder, 
              private authService: AuthService, 
              private router: Router,
              public toastr : ToastrService
             ) 
          {
            this.loginForm = this.fb.group({
                email: ["", [Validators.required,Validators.pattern(regex.emailPattern)]],
                password: ["", [Validators.required]]
            })
         } 

  get f(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  ngOnInit(): void {
    
  }

  login(){
    this.isSubmitted = true
    if (!this.loginForm.valid) {
      return;
    }

    let data = {
      email : this.loginForm.value.email,
      password: this.loginForm.value.password
    }

    this.authService.login(data).subscribe({next:(res: any) => {
      if (res.status === constVariable.SUCCESS) {
        localStorage.setItem("token" , res.token)
        this.toastr.success(res.message)
        this.router.navigateByUrl("audio")
      } else {
        this.toastr.error(res.message)
      }
    },
    error: (err:any) => {
      this.toastr.error(err)
    }
    })
  }
}
