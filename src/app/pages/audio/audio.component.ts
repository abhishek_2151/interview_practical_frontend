import { Component , OnInit , ElementRef , ViewChild} from '@angular/core';
import { AudioService } from 'src/app/core/http/api/audio.service';
import { constVariable } from 'src/environments/const';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-audio',
  templateUrl: './audio.component.html',
  styleUrls: ['./audio.component.css']
})
export class AudioComponent implements OnInit {
  @ViewChild('audioPlayer')audioPlayer!: ElementRef;


  audioData:any = []
  isPlaying: boolean = false;

  constructor(
              private audioService: AudioService,
              public toastr: ToastrService
            ) 
          { } 

  ngOnInit(): void {
    this.getAllAudio()
  }

  getAllAudio(){
    this.audioService.getAllAudio().subscribe({next:(res: any) => {
      if (res.status === constVariable.SUCCESS) {

        this.audioData = res.data
      } else {
        this.toastr.error(res.message)
      }
    },
    error: (err:any) => {
      this.toastr.error(err)
    }
    })
  }

  toggleAudio() {
    const audio: HTMLAudioElement = this.audioPlayer.nativeElement;

    if (this.isPlaying) {
      audio.pause();
    } else {
      audio.play();
    }

    this.isPlaying = !this.isPlaying;
  }
}
