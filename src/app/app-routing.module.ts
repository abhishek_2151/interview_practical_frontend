import { AuthGuardService } from './core/auth/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { AudioComponent } from './pages/audio/audio.component';

const routes: Routes = [
  { path: 'audio' ,canActivate: [AuthGuardService], component:AudioComponent},
  { path: 'login' , component:LoginComponent},
  { path: '', component: LoginComponent},
  { path: '*', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
