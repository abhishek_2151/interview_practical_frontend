import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, Data } from '@angular/router';
import { Observable } from 'rxjs';
import { GlobalService } from '../utils/global.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(private router: Router, private globalService: GlobalService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const IsLogin = this.globalService.isUserLoggedIn();
    if (IsLogin === true) {
      return true;
    } else {
      const urlPath = route.url[0].path;
      this.router.navigateByUrl('login')
      return false;
    }
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return false;
  }
}
