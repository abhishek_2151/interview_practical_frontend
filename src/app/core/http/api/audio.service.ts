import { Injectable } from '@angular/core';
import { HttpService  } from '../http.service'
import { endPoints } from 'src/environments/endpoints'

@Injectable({
  providedIn: 'root'
})
export class AudioService {

  constructor(private httpService: HttpService) { }


  addAudio: any = (data:any) => {
    return this.httpService.post(`${endPoints.addAudio}`, data)
  }

  getAllAudio: any = () => {
    return this.httpService.get(`${endPoints.getAllAudio}`)
  }
}
