import { Injectable } from '@angular/core';
import { HttpService  } from '../http.service'
import { endPoints } from 'src/environments/endpoints'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpService: HttpService) { }


  login: any = (data:any) => {
    return this.httpService.postLogin(`${endPoints.userLogin}`, data)
  }
}
