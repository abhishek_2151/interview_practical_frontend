import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { GlobalService } from '../utils/global.service';
import { constVariable } from 'src/environments/const';
const API_URL = environment.apiUrl;


@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient, private globalService: GlobalService) { }

  getHeaders = () => {
    const token = this.globalService.getAuthToken();
    const headers = new HttpHeaders({
      'content-type': constVariable.APPLICATION_JSON , authorization: `${constVariable.BEARER} ${token}`,
      'Access-Control-Allow-Origin':'*'
    });
    return headers;
  }

  postLogin = (apiRoute: any, body: any) => {
    return this.httpClient.post(API_URL + apiRoute, body).pipe(catchError(this.handleError));
  }

  // post request
  post = (apiRoute: any, body: any) => {
    const headers = this.getHeaders();
    return this.httpClient.post(API_URL + apiRoute, body, { headers: headers }).pipe(catchError(this.handleError));
  }

  // get request
  get = (apiRoute: any , params?:any , header?:any) => {
    const headers = this.getHeaders();
    return this.httpClient.get(API_URL + apiRoute, { headers: headers, params: params }).pipe(catchError(this.handleError));
  }

  // put request
  put = (apiRoute: any, body: any) => {
    const headers = this.getHeaders();
    return this.httpClient.put(API_URL + apiRoute, body, { headers: headers }).pipe(catchError(this.handleError));
  }

  // delete request
  delete = (apiRoute: any) => {
    const headers = this.getHeaders();
    return this.httpClient.delete(API_URL + apiRoute, { headers: headers }).pipe(catchError(this.handleError));
  }

  postFormData = (apiRoute: any, body: any) => {
    const token = this.globalService.getAuthToken();
    const headers = new HttpHeaders({
      authorization: `${constVariable.BEARER} ${token}`
    });
    return this.httpClient.post(API_URL + apiRoute, body, { headers: headers }).pipe(catchError(this.handleError));
  }

  // handler error
  handleError = (error: HttpErrorResponse) => {
    let err = (error && error.error) ? error.error : error; 
    if (error.status === 401 || error.status === 500) {
      this.globalService.unauthorizeUser();
    }
    return throwError(err.message);
  }

}
