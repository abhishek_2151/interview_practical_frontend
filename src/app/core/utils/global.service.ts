import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { constVariable } from 'src/environments/const';


@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor(private router: Router) { }

  getAuthToken = () => {
    return localStorage.getItem("token");
  }

  unauthorizeUser = () => {
    localStorage.clear()
    this.router.navigateByUrl(``);
  }

  isUserLoggedIn = () => {
    const token = localStorage.getItem("token");
    if (token !== undefined && token != null) {
      return true;
    } else {
      return false;
    }
  }
}
